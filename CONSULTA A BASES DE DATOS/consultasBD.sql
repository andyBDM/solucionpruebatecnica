/*
PROYECTOS  ( CODIGOP :STRING, DESCRIPCION:STRING, MUNICIPIO:STRING, CLIENTE:STRING)
MAQUINAS   ( CODIGOM :STRING, NOMBRE:STRING, PRECIO_HORA:DOUBLE)
CONDUCTORES( CODIGOC :STRING, NOMBRE:STRING, MUNICIPIO:STRING, CATEGORIA:INT)
TRABAJOS   ( CODIGC  :STRING, CODIGOM :STRING, CODIGOP :STRING, FECHA :DATE, TIEMPO:INT)
*/

-- 1. Obtener el nombre y el municipio de los  conductores que no trabajan en los proyectos
-- de Henry Pulido
SELECT CO.NOMBRE, CO.MUNICIPIO
  FROM PROYECTOS PR 
    INNER JOIN TRABAJOS TR ON PR.CODIGOP = TR.CODIGOP
    INNER JOIN CONDUCTORES CO ON TR.CODIGC = CO.CODIGOC
  WHERE PR.CLIENTE NOT LIKE 'Henry Pulido'
;

-- 2.  Obtener por cada uno de los proyectos existentes, la descripción del proyecto, el
-- cliente y el total a facturar. Ordenar el resultado por cliente y total a facturar.
SELECT PRO.DESCRIPCION DESC_PROYECTO, PRO.CLIENTE, SUM(CAL.VALOR) AS TOTAL_FACTURAR
  FROM PROYECTOS PRO
  INNER JOIN (
    SELECT PR.CODIGOP AS PROYECTO, TR.TIEMPO*MA.PRECIO_HORA AS VALOR
      FROM PROYECTOS PR
      INNER JOIN TRABAJOS TR ON  TR.CODIGOP = PR.CODIGOP
      INNER JOIN MAQUINAS MA ON  TR.CODIGOM = MA.CODIGOM
  ) CAL ON PRO.CODIGOP = CAL.PROYECTO
GROUP BY PRO.DESCRIPCION, PRO.CLIENTE
ORDER BY 2, 3;

-- 3. Obtener el municipio cuyos conductores (al menos uno) haya participado en más de
-- dos proyectos diferentes

 /* Consulta que obtiene los municipios de los conductores */
SELECT SUB_MUN.MUN
FROM (
  SELECT DISTINCT TR.CODIGC , CO.MUNICIPIO MUN
    FROM TRABAJOS TR
      INNER JOIN (
      SELECT CONDUCTOR, COUNT(*) NUM_PROY
        FROM (
          SELECT CODIGC CONDUCTOR, CODIGOP
            FROM TRABAJOS
          GROUP BY CODIGC, CODIGOP
          ) GROUP BY CONDUCTOR
      ) AUX ON TR.CODIGC = AUX.CONDUCTOR
      INNER JOIN CONDUCTORES CO ON  TR.CODIGC = CODIGOC 
    WHERE NUM_PROY > 2
    ) SUB_MUN    
;

 /* Consulta que obtiene los municipios de los proyectos */ 
SELECT DISTINCT MPO_CON.MUNICIPIO
  FROM (
    SELECT  MUN_PRO.COND CONDUCTOR, MUN_PRO.MUNI MUNICIPIO, COUNT(*) R
      FROM (
        SELECT TR.CODIGC COND, PR.CODIGOP PROY, PR.MUNICIPIO MUNI
          FROM TRABAJOS TR
          INNER JOIN PROYECTOS PR ON TR.CODIGOP = PR.CODIGOP
        GROUP BY TR.CODIGC, PR.CODIGOP, PR.MUNICIPIO
      )MUN_PRO
    GROUP BY MUN_PRO.COND, MUN_PRO.MUNI
    ) MPO_CON
    WHERE  R>2  
;

-- 4. Subir el precio por hora en un 10% del precio por hora más bajo para todas las
-- máquinas excepto para aquella que tenga el valor más alto
UPDATE MAQUINAS
  SET PRECIO_HORA = 
    PRECIO_HORA + (SELECT MIN(PRECIO_HORA)*0.1 AUMENTO
      FROM  MAQUINAS
    )
  WHERE PRECIO_HORA NOT LIKE (
    SELECT MAX(PRECIO_HORA) MAXIMO
  FROM  MAQUINAS)
;

-- 5. Crear una vista que contenga el nombre del conductor, la descripción del proyecto y la
-- media aritmética del tiempo trabajado

CREATE VIEW VISTA_CONDUCTOR AS
  SELECT CO.NOMBRE, PR.DESCRIPCION PROYECTO, ROUND(AVG(TR.TIEMPO),2) AS PROMEDIO
    FROM TRABAJOS TR
      INNER JOIN PROYECTOS PR ON PR.CODIGOP = TR.CODIGOP
      INNER JOIN CONDUCTORES CO ON CO.CODIGOC = TR.CODIGC
    GROUP BY CO.NOMBRE, PR.DESCRIPCION
    ORDER BY 1,2
;
SELECT * FROM VISTA_CONDUCTOR;
