var seg;
var min;
var hor;

var horaLLamada;
var horaFin;
var numeroLlamdas = 1;
var timer;

var durSegTigo = 0;
var durMinTigo = 0;
var durHorTigo = 0;

var durSegMov = 0;
var durMinMov = 0;
var durHorMov = 0;

var durSegClaro = 0;
var durMinClaro = 0;
var durHorClaro = 0;

var durSegVirgin = 0;
var durMinVirgin = 0;
var durHorVirgin = 0;

var ban;

function llamar(){
    seg = 0;
    min = 0;
    hor = 0;

    validar();
    horaLLamada=new Date();
    horaLLamada=horaLLamada.getHours()+":"+horaLLamada.getMinutes()+":"+horaLLamada.getSeconds(); 
   
    timer = setInterval(function(){ 
        if(seg == 60){
            min++;
            seg = 0;
        } 
        if(min == 60){
            hor++;
            min = 0;
        }  
        seg++;
        var tiempo = document.getElementById("tiempo").innerHTML = hor +" : "+ min +" : " + seg;
        console.log("Minutos:"+ min  +"- Segundos: "+ seg);
        },1000);
    
}

function colgar(){
    clearTimeout(timer);
    horaFin=new Date();
    horaFin=horaFin.getHours()+":"+horaFin.getMinutes()+":"+horaFin.getSeconds(); 
    calcularLLamdas();
    cacularConsolidado();
    seg = 0;
    min = 0;
    hor = 0;
    var tiempo = document.getElementById("tiempo").innerHTML = hor +" : "+ min +" : " + seg;
}

function calcularLLamdas(){
    
    var operador = document.getElementById("operador");
    var indicativo = operador.options[operador.selectedIndex].text;
    var numero = document.getElementById("numero").value;
    var costoMin;
    var costoTotal = 0;

    if(operador.value == "TIGO"){
        costoMin = 120;
        durSegTigo = seg;
        if(durSegTigo>60){
            durMinTigo++;
            durSegTigo=0;
        }  
        durMinTigo = min;
        if(durMinTigo>60){
            durHorTigo++;
            durMinTigo=0;
        }  
        durHorTigo = hor;
    }
        
    if(operador.value == "MOVISTAR"){
        costoMin = 180;
        durSegMov = seg;
        if(durSegMov>60){
            durMinMov++;
            durSegMov = 0;
        }
            
        durMinMov = min;
        if(durMinMov>60){
            durHorMov++;
            durMinMov = 0;
        }
            
        durHorMov = hor;
    }
        
    if(operador.value == "CLARO"){
        durSegClaro = seg;
        if(durSegClaro>60){
            durMinClaro++;
            durSegClaro = 0;
        }  
        durMinClaro = min;
        if(durMinClaro>60){
            durHorClaro++;
            durMinClaro = 0;
        }  
        durHorClaro = hor;
        costoMin = 210;
    }
        
    if(operador.value == "VINGIN"){
        costoMin = 220;
        durSegVirgin = seg;
        if(durSegVirgin>60){
            durMinVirgin++;
            durSegVirgin = 0;
        }   
        durMinTigo = min;
        if(durMinVirgin>60){
            durHorVirgin++;
            durMinVirgin = 0;
        } 
        durHorVirgin = hor;
    }
        

    if(hor>0)
        costoTotal += (hor*60)*costoMin;
    if(min>0)
        costoTotal += min*costoMin;
    if(seg>0)
        costoTotal += costoMin;

    var table = document.getElementById("llamadas").innerHTML+=
    "<tr><td>"+numeroLlamdas +"</td>"+
        "<td>"+operador.value +"</td>"+
        "<td>"+indicativo.value + numero +"</td>"+
        "<td>"+horaLLamada +"</td>"+
        "<td>"+horaFin +"</td>"+
        "<td>"+min +" : " +seg +"</td>"+
        "<td>"+costoMin +"</td>"+
        "<td>"+costoTotal +"</td>"+
    "</tr>";

    numeroLlamdas++;
}

function cacularConsolidado(){
    var costoTigo = 0;
    var costoClaro = 0;
    var costoMovistar = 0;
    var costoVirgin = 0;

    if(durHorTigo>0)
        costoTigo += (durHorTigo*60)*120;
    if(durMinTigo>0)
        costoTigo += durMinTigo * 120;
    if(durSegTigo>0)
        costoTigo += 120;

    if(durHorClaro>0)
        costoClaro += (durHorClaro*60)*210;
    if(durMinClaro>0)
        costoClaro += durMinClaro * 210;
    if(durSegClaro>0)
        costoClaro += 210;

    if(durHorMov>0)
        costoMovistar += (durHorMov*60)*180;
    if(durMinMov>0)
        costoMovistar += durMinMov * 180;
    if(durSegMov>0)
        costoMovistar += 180;

    if(durHorVirgin>0)
        costoVirgin += (durHorVirgin*60)*220;
    if(durMinVirgin>0)
        costoVirgin += durMinVirgin * 220;
    if(durSegVirgin>0)
        costoVirgin += 220;

    var table = document.getElementById("consolidado").innerHTML=
    "<tr><th>Operador</th>"+
       "<th>Duración</th>"+
        "<th>Costo</th>"+
    "</tr>"+
    "<tr><td>Claro</td>"+
        "<td>"+durHorClaro +":"+durMinClaro+":"+durSegClaro +"</td>"+
        "<td>"+costoClaro+"</td>"+    
    "</tr>"+
        "<tr><td>Movistar</td>"+
        "<td>"+durHorMov +":"+durMinMov+":"+durSegMov +"</td>"+
        "<td>"+ costoMovistar+"</td>"+    
    "</tr>"+
        "<tr><td>Tigo</td>"+
        "<td>"+durHorTigo +":"+durMinTigo+":"+durSegTigo +"</td>"+
        "<td>"+ costoTigo+"</td>"+    
    "</tr>"+
        "<tr><td>Virgin</td>"+
        "<td>"+durHorVirgin +":"+durMinVirgin+":"+durSegVirgin +"</td>"+
        "<td>"+ costoVirgin+"</td>"+    
    "</tr>";

    var costoTotal = document.getElementById("costoTotal").innerHTML = costoClaro+costoTigo+costoMovistar+costoVirgin;
}
function validar(){
    var numero = document.getElementById("numero");
    var expRegNumero = /[0-9]{7,7}$/; 

    if(!numero.value){
        alert("Por favor digitar numero");
        PIN.focus();
        ban = false;
    } 
    if(!expRegNumero.exec(numero.value)) {
        alert("PIN incorrecto");
        PIN.focus();
    }   
    else
        ban = true; 
}

window.onload = function(){

    var botonLLamar = document.getElementById("llamar");
    var botonColgar = document.getElementById("colgar");

    botonLLamar.onclick	= llamar;
    botonColgar.onclick = colgar;
;}
    