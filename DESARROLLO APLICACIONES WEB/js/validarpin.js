function validarIngreso(){
   
    var expRegPin = /^[A-Z]{4}[a-z]{4}[0-9]$/;;
    var PIN = document.getElementById("pin");
    if(!PIN){
        alert("Por favor digitar su PIN");
        PIN.focus();
    }
    else if(!expRegPin.exec(PIN.value)) {
        alert("PIN incorrecto");
        PIN.focus();
    }    
    else
        location.href="./registrollamada.html"    
}

window.onload = function(){
    var botonIngresar = document.getElementById("ingresar");
    botonIngresar.onclick = validarIngreso;
}
